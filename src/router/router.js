/**
 * 全站路由配置
 *
 * meta参数说明
 * keepAlive是否缓冲页面
 * isTab是否加入到tag导航
 * isAuth是否需要授权
 */
import VueRouter from "vue-router";
import PageRouter from "./self-routes";
import {autoRoutes} from './auto-routes';

let Router = new VueRouter({
  // mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      if (from.meta.keepAlive) {
        from.meta.savedPosition = document.body.scrollTop;
      }
      return {
        x: 0,
        y: to.meta.savedPosition || 0,
      };
    }
  },
  routes: [],
});
// AvueRouter.install(Vue, Router, Store, i18n);
// Router.$avueRouter.formatRoutes(Store.state.user.menu, true);
Router.addRoutes([...autoRoutes,...PageRouter]);
export default Router;
