import Layout from "@/page/admin/";
import website from '@/config/website'
export default [

  {
    path: "/oauth/redirect/:source",
    component: () =>
      import(/* webpackChunkName: "page" */ "@/components/redirect/index"),
    name: "第三方认证跳转页",
    meta: {
      keepAlive: true,
      isTab: false,
      isAuth: false,
    },
  },
  {
    path: "/oauth/code",
    component: () =>
      import(/* webpackChunkName: "page" */ "@/components/redirect/code"),
    name: "授权码登陆",
    meta: {
      keepAlive: true,
      isTab: false,
      isAuth: false,
    },
  },
  {
    path: "/lock",
    name: "锁屏页",
    component: () => import(/* webpackChunkName: "page" */ "@/page/lock/index"),
    meta: {
      keepAlive: true,
      isTab: false,
      isAuth: false,
    },
  },
  {
    path: "*",
    component: Layout,
    redirect: "/403",
    children: [
      {
        path:"/403",
        component: () =>
          import(/* webpackChunkName: "page" */ "@/components/error-page/403"),
        name: "403",
        meta: {
          keepAlive: true,
          isTab: false,
          isAuth: false,
        },
      },
    ],

  },
  {
    path: "/500",
    component: () =>
      import(/* webpackChunkName: "page" */ "@/components/error-page/500"),
    name: "500",
    meta: {
      keepAlive: true,
      isTab: false,
      isAuth: false,
    },
  },

  {
    // 嵌套页专用
    path: "/myiframe",
    component: Layout,
    redirect: "/myiframe",
    children: [
      {
        path: ":routerPath",
        name: "iframe",
        component: () =>
          import(/* webpackChunkName: "page" */ "@/components/iframe/main"),
        props: true,
      },
    ],
  },
];
