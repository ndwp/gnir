
import website from '@/config/website'
import { validatenull } from "@/util/validate"
// 引入 所有View页面
const views = require.context('@/views',true, /.vue$/)
// 引入所有布局页面 规则为下面的 所有index.vue 页面
const pages = require.context('@/page', true, /index.vue$/)

let defLay = website.defaultLayout;

// 根据布局获取页面默认 admin 布局
function getViewRouter(pageKey) {
  const router = []
  views.keys().forEach(key => {
    const path= key.replace(/^\.*|\.vue*$/g,'');
    let component = views(key).default || views(key)

    if(!component.layout) {
      component.layout = website.defaultLayout
    }
    if(pageKey === component.layout) {
      router.push({
        path: path,
        component(resolve){
          require([`../views${path}`], resolve)
        return
        }
      })
    }
  })
  return router
}
// 组合路由
const layouts = []
pages.keys().forEach(key => {
  const pageKey= key.replace(/^\.\/*|\/index\.vue*$/g,'');
  let children = getViewRouter(pageKey)

  if(!validatenull(children)) {
    layouts.push({
      path: pageKey===defLay?"/":"/"+pageKey,
      component(resolve){
        require([`../page/${pageKey}`], resolve)
        return
      } ,
      redirect: pageKey===defLay?website.fistPage.value:"/"+pageKey+website.fistPage.value,
      children
    })
  }else {
    layouts.push({
      path:"/"+pageKey,
      component(resolve){
        require([`../page/${pageKey}`], resolve)
        return
      },
      meta: {
        keepAlive: true,
        isTab: false,
        isAuth: false,
      },
    })
  }

})


export const autoRoutes = layouts;
export const routeTools = {
  getValue: function(route) {
    let value = "";
    if (route.query.src) {
      value = route.query.src;
    } else {
      value = route.path;
    }
    return value;
  },
}

