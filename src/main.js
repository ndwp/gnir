import Vue from "vue";
import axios from "./router/axios";
import VueAxios from "vue-axios";
import App from "@/App";
import router from "./router/router";
import "./permission"; // 权限
import "./error"; // 日志
import store from "./store";
import { loadStyle } from "./util/util";
import * as urls from "@/config/env";
import Element from "element-ui";
// import VueJboot from "vue-jboot"; // 使用已发布的vue-jboot库
import VueJboot from "./lib"; // 使用本地vue-jboot库
import { iconfontUrl, iconfontVersion } from "@/config/env";
import i18n from "./lang"; // Internationalization
import "./styles/common.scss";
import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
import AvueFormDesign from 'avue-form-design'
import AvueUeditor from 'avue-plugin-ueditor'
Vue.use(AvueFormDesign)
Vue.use(AvueUeditor)
Vue.use(Avue,{axios});
Vue.use(router);
Vue.use(VueAxios, axios);
Vue.use(Element, {
  i18n: (key, value) => i18n.t(key, value),
});
// 全局引用VueJboot
Vue.use(VueJboot);
// Vue.use(window.AVUE, {
//   i18n: (key, value) => i18n.t(key, value),
// });

// 加载相关url地址
Object.keys(urls).forEach((key) => {
  Vue.prototype[key] = urls[key];
});

// 动态加载阿里云字体库
iconfontVersion.forEach((ele) => {
  loadStyle(iconfontUrl.replace("$key", ele));
});

Vue.config.productionTip = false;
Vue.prototype.$L = window.L
new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
