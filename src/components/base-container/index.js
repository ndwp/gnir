import BaseContainer from './src/main.vue'
/* istanbul ignore next */
BaseContainer.install = function(Vue) {
  Vue.component(BaseContainer.name, BaseContainer);
};
export default BaseContainer;
