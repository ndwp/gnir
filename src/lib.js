/*
 * vue-jboot 组件库入口，这个库会发布到私服
 */

// 工具
import autolog from "./util/console.js";
import request from "./router/axios";
import { validatenull } from "./util/validate";
import { findObject } from "./util/util";
// 组件
import BaseContainer from "@/components/base-container/index.js";

const components = [
  BaseContainer,
  // JbootCanvas,
];
const install = function(Vue, opts = {}) {
  components.forEach((component) => {
    Vue.component(component.name, component);
  });
  Vue.prototype.$console = autolog;
  Vue.prototype.$axios = request;
  Vue.prototype.$validatenull = validatenull;
  Vue.prototype.$findObject = findObject;
};

/* istanbul ignore if */
if (typeof window !== "undefined" && window.Vue) {
  install(window.Vue);
}
export default {
  version: "1.2.0",
  install,
  ...components
};
