import {env} from "./env";

/**
 * 全局配置文件
 */
export default {
  title: "项目考核系统",
  clientId: "web", // 客户端id
  clientSecret: "web@123", // 客户端密钥
  tenantMode: true, // 是否开启租户模式
  captchaMode: env.NODE_ENV === "production", // 是否开启验证码模式
  rememberMode: true, // 是否开启记住密码模式
  logo: "G",
  key: "gungnir", //配置主键,目前用于存储
  lockPage: "/lock",
  tokenTime: 6000,
  //http的status默认放行不才用统一处理的,
  statusWhiteList: [],
  // 配置首页不可关闭
  isFirstPage: false,
  defaultLayout: 'admin',
  fistPage: { // 登陆后跳转的页面
    label: "首页",
    value: "/admin/index",
    meta: {
      i18n: "console",
      $keepAlive: false,
    },
    // label: "门户首页",
    // value: "/home/index",
    // meta: {
    //   i18n: "dashboard",
    //   $keepAlive: false,
    // },
    params: {},
    query: {},
    group: [],
    close: false,
  },

  //配置菜单的属性
  menu: {
    iconDefault: "iconfont icon-caidan",
    props: {
      label: "name",
      path: "path",
      icon: "source",
      children: "children",
    },
  },
};
