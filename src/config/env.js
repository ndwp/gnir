// 配置编译环境和线上环境之间的切换

const env = process.env;
let baseUrl = env.VUE_APP_API_URL;
let mockUrl = baseUrl;
let iconfontVersion = ["567566_pwc3oottzol", "1066523_v8rsbcusj5q"];
let iconfontUrl = `//at.alicdn.com/t/font_$key.css`;
let codeUrl = `${baseUrl}/code`;
let tidituToken = "715330f549cf40030c5e7c23834ed987";


export {
  baseUrl,
  iconfontUrl,
  iconfontVersion,
  codeUrl,
  env,
  mockUrl,
  tidituToken,
};
