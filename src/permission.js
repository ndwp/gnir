/**
 * 全站权限配置
 *
 */
import router from './router/router'
import store from './store'
import {validatenull} from '@/util/validate'
import {getToken} from '@/util/auth'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
NProgress.configure({showSpinner: false});
const lockPage = store.getters.website.lockPage; //锁屏页
// const topUrl = getTopUrl();
// const redirectUrl = "/oauth/redirect/";
router.beforeEach((to, from, next) => {
  // if (to.matched.length === 0 && to.fullPath.indexOf("?sec") === -1) {
  //   next(to.path + "?sec");
  //   window.location.reload();
  // } else {
  //   next();
  // }
  // next()
  // debugger
  const meta = to.meta || {};
  if (getToken()) {
    const value = to.query.src || to.fullPath;
    const label = to.query.name || to.name;
    const meta = to.meta || router.meta || {};
    const i18n = to.query.i18n;
    store.commit('ADD_TAG', {
      label: label,
      value: value,
      params: to.params,
      query: to.query,
      meta: (() => {
        if (!i18n) {
          return meta
        }
        return {
          i18n: i18n
        }
      })()
    });
    next()
  } else {
    // next('/login')
    //判断是否需要认证，没有登录访问去登录页
    if (meta.isAuth === false) {
      next()
    } else {
      next('/login')
    }
  }
})

router.afterEach(() => {
  NProgress.done();
});
