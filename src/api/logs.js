import request from "@/router/axios";
import { baseUrl } from "@/config/env";
export const getGeneralList = (current, size) => {
  return request({
    url: baseUrl + "/log-general/list",
    method: "get",
    params: {
      current,
      size,
    },
  });
};

export const getApiList = (current, size) => {
  return request({
    url: baseUrl + "/log-api/list",
    method: "get",
    params: {
      current,
      size,
    },
  });
};

export const getErrorList = (current, size) => {
  return request({
    url: baseUrl + "/log-error/list",
    method: "get",
    params: {
      current,
      size,
    },
  });
};

export const getGeneralLogs = (id) => {
  return request({
    url: baseUrl + "/log-general/detail",
    method: "get",
    params: {
      id,
    },
  });
};
export const getApiLogs = (id) => {
  return request({
    url: baseUrl + "/log-api/detail",
    method: "get",
    params: {
      id,
    },
  });
};
export const getErrorLogs = (id) => {
  return request({
    url: baseUrl + "/log-error/detail",
    method: "get",
    params: {
      id,
    },
  });
};
