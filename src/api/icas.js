/**
 * Copyright (c) 2020-2030, Zhiqiangc 程志强 (zhiqiangc1991@outlook.com).
 * <p>
 */


/**
 * 第三方账号登陆
 * @author     ：Zhiqiangc.
 * @date       ：Created in 11:13 2021/2/23
 */
import request from "@/router/axios";
import { baseUrl } from "@/config/env";

// 获取授权地址
export const oauthRender = (source) => {
  return request({
    url: `${baseUrl}/oauth/render/${source}`,
    method: "get", // 所有类型都可以
  });
};
// 获取认证信息
export const oauthCallback = (source,code,auth_code,state,authorization_code,oauth_token,oauth_verifier) => {
  return request({
    url: `${baseUrl}/oauth/callback/${source}`,
    method: "get", // 所有类型都可以
    params: {
      code,auth_code,state,authorization_code,oauth_token,oauth_verifier
    }
  });
};
// 取消授权
export const oauthRevoke = (source,token) => {
  return request({
    url: `${baseUrl}/oauth/revoke/${source}/${token}`,
    method: "get", // 所有类型都可以
  });
};
// 令牌续期
export const oauthRefresh = (source,token) => {
  return request({
    url: `${baseUrl}/oauth/refresh/${source}`,
    method: "get", // 所有类型都可以
    params:{
      token
    }
  });
};
