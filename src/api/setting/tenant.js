import request from "@/router/axios";
import { baseUrl } from "@/config/env";
export const getList = (current, size, params) => {
  return request({
    url: baseUrl + "/tenant/list",
    method: "get",
    params: {
      ...params,
      current,
      size,
    },
  });
};
export const remove = (ids) => {
  return request({
    url: baseUrl + "/tenant/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: baseUrl + "/tenant/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: baseUrl + "/tenant/submit",
    method: "post",
    data: row,
  });
};
export const getDetail = (id) => {
  return request({
    url: baseUrl + "/tenant/detail",
    method: "get",
    params: {
      id,
    },
  });
};

export const getTenantSelect = () => {
  return request({
    url: baseUrl + "/tenant/select",
    method: "get",
  });
};
