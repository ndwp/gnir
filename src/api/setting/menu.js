import request from "@/router/axios";
import { baseUrl } from "@/config/env";
export const getList = (current, size, params) => {
  return request({
    url: baseUrl + "/menu/list",
    method: "get",
    params: {
      ...params,
      current,
      size,
    },
  });
};
export const remove = (ids) => {
  return request({
    url: baseUrl + "/menu/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: baseUrl + "/menu/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: baseUrl + "/menu/submit",
    method: "post",
    data: row,
  });
};

export const getMenu = (id) => {
  return request({
    url: baseUrl + "/menu/detail",
    method: "get",
    params: {
      id,
    },
  });
};

export const getMenuTree = () => {
  return request({
    url: baseUrl + "/menu/tree",
    method: "get",
  });
};
