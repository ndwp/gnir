import request from "@/router/axios";
import { baseUrl } from "@/config/env";
export const getList = (current, size, params) => {
  return request({
    url: baseUrl + "/dept/list",
    method: "get",
    params: {
      ...params,
      current,
      size,
    },
  });
};
export const remove = (ids) => {
  return request({
    url: baseUrl + "/dept/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: baseUrl + "/dept/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: baseUrl + "/dept/submit",
    method: "post",
    data: row,
  });
};

export const getDept = (id) => {
  return request({
    url: baseUrl + "/dept/detail",
    method: "get",
    params: {
      id,
    },
  });
};
export const getDeptTree = (tenantId) => {
  return request({
    url: baseUrl + "/dept/tree",
    method: "get",
    params: {
      tenantId,
    },
  });
};
