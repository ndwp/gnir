import request from "@/router/axios";
import { baseUrl } from "@/config/env";
export const getList = (current, size, params) => {
  return request({
    url: baseUrl + "/dict/list",
    method: "get",
    params: {
      ...params,
      current,
      size,
    },
  });
};
export const remove = (ids) => {
  return request({
    url: baseUrl + "/dict/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: baseUrl + "/dict/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: baseUrl + "/dict/submit",
    method: "post",
    data: row,
  });
};

export const getDetail = (id) => {
  return request({
    url: baseUrl + "/dict/detail",
    method: "get",
    params: {
      id,
    },
  });
};
export const getDictTree = () => {
  return request({
    url: baseUrl + "/dict/tree?code=DICT",
    method: "get",
  });
};
export const getDict = (code) => {
  return request({
    url: baseUrl + "/dict/dictionary",
    method: "get",
    params: {
      code,
    },
  });
};
