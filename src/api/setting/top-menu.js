import request from "@/router/axios";
import { baseUrl } from "@/config/env";
export const getList = (current, size, params) => {
  return request({
    url: baseUrl + "/top-menu/list",
    method: "get",
    params: {
      ...params,
      current,
      size,
    },
  });
};
export const remove = (ids) => {
  return request({
    url: baseUrl + "/top-menu/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: baseUrl + "/top-menu/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: baseUrl + "/top-menu/submit",
    method: "post",
    data: row,
  });
};

export const getTopMenu = (id) => {
  return request({
    url: baseUrl + "/top-menu/detail",
    method: "get",
    params: {
      id,
    },
  });
};
export const getGrantKeys = (topMenuIds) => {
  return request({
    url: baseUrl + "/top-menu/menu-keys",
    method: "get",
    params: {
      topMenuIds,
    },
  });
};
export const grant = (topMenuIds,menuIds) => {
  return request({
    url: baseUrl + "/top-menu/grant",
    method: "post",
    params: {
      topMenuIds,menuIds
    },
  });
};
