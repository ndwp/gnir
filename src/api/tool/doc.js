import request from "@/router/axios";
import { baseUrl } from "@/config/env";
export const getV2Doc = () => {
  return request({
    url: baseUrl + "/v2/api-docs",
    method: "get",
  });
};

export const debugWebapi = (url,method,params,data={}) => {
    return request({
      url: baseUrl + url,
      method,
      params,
      data,
    });
  };