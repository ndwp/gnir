import request from "@/router/axios";
import { baseUrl } from "@/config/env";
export const genProject = (params) => {
  return request({
    url: baseUrl + "/develop/project",
    method: "get",
    params,
    responseType: "blob",
  });
};
export const genCode = (params) => {
  return request({
    url: baseUrl + "/develop/code",
    method: "get",
    params,
    responseType: "blob",
  });
};
// url 中pdf.html 为后端模版文件名
// 可根据实际情况修改，
export const genPDF = (data, template = "pdf/pdf.html") => {
  return request({
    url: `${baseUrl}/develop/pdf/`,
    method: "post",
    params: {
      template,
    },
    data,
    responseType: "blob",
  });
};
