import request from "@/router/axios";
import { baseUrl } from "@/config/env";
import website from "@/config/website";

export const loginByUsername = (tenantId, account, password, type, key, code) =>
  request({
    url: baseUrl + "/token",
    method: "post",
    headers: {
      "Captcha-Key": key,
      "Captcha-Code": code,
    },
    params: {
      grantType: website.captchaMode ? "captcha" : "password",
      tenantId,
      account,
      password,
      type,
    },
  });
export const loginByIcas = (tenantId, source, code, state) => request({
  url: baseUrl + "/token",
  method: 'post',
  headers: {
    'Tenant-Id': tenantId
  },
  params: {
    tenantId,
    source,
    code,
    state,
    grantType: "icas-sns",
    scope: "all",
  }
});
export const registerGuest = (form, oauthId) => request({
  url: baseUrl +'/user/register-guest',
  method: 'post',
  params: {
    tenantId: form.tenantId,
    name: form.name,
    account: form.account,
    password: form.password,
    oauthId
  }
});
export const getButtons = () =>
  request({
    url: baseUrl + "/menu/buttons",
    method: "get",
  });

export const getUserInfo = () =>
  request({
    url: baseUrl + "/user/getUserInfo",
    method: "get",
  });

export const refeshToken = () =>
  request({
    url: baseUrl + "/user/refesh",
    method: "post",
  });

export const getMenu = (topMenuId = 0) =>
  request({
    url: baseUrl + "/menu/routes",
    method: "get",
    params: {
      topMenuId,
    },
  });

export const getCaptcha = () =>
  request({
    url: baseUrl + "/captcha",
    method: "get",
  });

export const getTopMenu = () =>
  request({
    // url: "/user/getTopMenu.json",
    url: baseUrl + "/menu/top-menu",
    method: "get",
  });

export const sendLogs = (list) =>
  request({
    url: baseUrl + "/user/logout",
    method: "post",
    data: list,
  });

export const logout = () =>
  request({
    url: baseUrl + "/user/logout",
    method: "get",
  });
