import request from "@/router/axios";
import { baseUrl } from "@/config/env";
export const getList = (current, size, params) => {
  return request({
    url: baseUrl + "/role/list",
    method: "get",
    params: {
      ...params,
      current,
      size,
    },
  });
};
export const grantTree = () => {
  return request({
    url: baseUrl + "/menu/grant-tree",
    method: "get",
  });
};

export const grant = (roleIds, menuIds,apiScopeIds,dataScopeIds) => {
  return request({
    url: baseUrl + "/role/grant",
    method: "post",
    params: {
      roleIds,
      menuIds,
      apiScopeIds,
      dataScopeIds
    },
  });
};

export const remove = (ids) => {
  return request({
    url: baseUrl + "/role/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const getDetail = (id) => {
  return request({
    url: baseUrl + "/role/detail",
    method: "get",
    params: {
      id,
    },
  });
};

export const add = (row) => {
  return request({
    url: baseUrl + "/role/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: baseUrl + "/role/submit",
    method: "post",
    data: row,
  });
};

export const getRole = (roleIds) => {
  return request({
    url: baseUrl + "/menu/role-tree-keys",
    method: "get",
    params: {
      roleIds,
    },
  });
};

export const getRoleTree = (tenantId) => {
  return request({
    url: baseUrl + "/role/tree",
    method: "get",
    params: {
      tenantId,
    },
  });
};
