import request from "@/router/axios";
import { baseUrl } from "@/config/env";

export const getList = (menuId,current, size, params) => {
  return request({
    url: baseUrl + "/power-api/list",
    method: "get",
    params: {
      menuId_numequal:menuId,
      ...params,
      current,
      size,
    },
  });
};

export const remove = (ids) => {
  return request({
    url: baseUrl + "/power-api/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: baseUrl + "/power-api/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: baseUrl + "/power-api/submit",
    method: "post",
    data: row,
  });
};

export const getDetail = (id) => {
  return request({
    url: baseUrl + "/power-api/detail",
    method: "get",
    params: {
      id,
    },
  });
};
