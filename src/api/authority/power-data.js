import request from "@/router/axios";
import { baseUrl } from "@/config/env";

export const getList = (menuId_numequal,current, size, params) => {
  return request({
    url: baseUrl + "/power-data/list",
    method: "get",
    params: {
      menuId_numequal,
      ...params,
      current,
      size,
    },
  });
};

export const remove = (ids) => {
  return request({
    url: baseUrl + "/power-data/remove",
    method: "post",
    params: {
      ids,
    },
  });
};

export const add = (row) => {
  return request({
    url: baseUrl + "/power-data/submit",
    method: "post",
    data: row,
  });
};

export const update = (row) => {
  return request({
    url: baseUrl + "/power-data/submit",
    method: "post",
    data: row,
  });
};

export const getDetail = (id) => {
  return request({
    url: baseUrl + "/power-data/detail",
    method: "get",
    params: {
      id,
    },
  });
};
