import request from '@/router/axios';
import { baseUrl } from '@/config/env'

export const getList = (current, size, params) => {
  return request({
    url: baseUrl + '/region/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getLazyTree = (parentCode, params) => {
  return request({
    url: baseUrl + '/region/lazy-tree',
    method: 'get',
    params: {
      ...params,
      parentCode
    }
  })
}

export const getDetail = (code) => {
  return request({
    url: baseUrl + '/region/detail',
    method: 'get',
    params: {
      code
    }
  })
}

export const remove = (ids) => {
  return request({
    url: baseUrl + '/region/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const submit = (row) => {
  return request({
    url: baseUrl + '/region/submit',
    method: 'post',
    data: row
  })
}

