import request from '@/router/axios'
import { baseUrl } from '@/config/env'

export const getList = (current, size, params) => {
  return request({
    url: baseUrl + '/simple/list',
    method: 'get',
    params: {
      ...params,
      current,
      size
    }
  })
}

export const remove = (ids) => {
  return request({
    url: baseUrl + '/simple/remove',
    method: 'post',
    params: {
      ids
    }
  })
}

export const add = (row) => {
  return request({
    url: baseUrl + '/simple/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: baseUrl + '/simple/submit',
    method: 'post',
    data: row
  })
}

export const getDetail = (id) => {
  return request({
    url: baseUrl + '/simple/detail',
    method: 'get',
    params: {
      id
    }
  })
}
