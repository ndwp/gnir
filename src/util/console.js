class AutoConsole {
  constructor() {
    this.env = process.env.NODE_ENV;
  }

  output(type, content) {
    switch (type) {
      case "log":
        window.console.log(content);
        break;
      case "warn":
        window.console.warn(content);
        break;
      case "error":
        window.console.error(content);
        break;
    }
  }

  log(content, mode = "normal") {
    if (this.env === "development" && mode === "normal") {
      this.output("log", content);
    } else {
    }
  }

  warn(content, mode = "normal") {
    if (this.env === "development" && mode === "normal") {
      this.output("warn", content);
    } else {
    }
  }

  error(content, mode = "normal") {
    if (this.env === "development" && mode === "normal") {
      this.output("error", content);
    } else {
    }
  }
}
const autolog = new AutoConsole();
export default autolog;
