import { getStore, setStore, removeStore } from "./store";
import { validatenull } from "@/util/validate";
import { calcDate } from "@/util/date";

export function getToken() {
  const token = getStore({
    name: "token",
    debug: true,
  });

  if (!token) {
    return;
  }
  const expiresIn = getStore({ name: "expiresIn" });
  const date = calcDate(token.datetime, new Date().getTime());
  if (validatenull(date)) return;
  if (date.seconds >= expiresIn) {
    return;
  }
  return token.content;
}

export function setToken(token) {
  return setStore({ name: "token", content: token });
  // const inFifteenMinutes = new Date(new Date().getTime() + 120 * 60 * 1000)
  // return Cookies.set(TokenKey, token, { expires: inFifteenMinutes })
}

export function removeToken() {
  return removeStore({ name: "token", type: true });
}
