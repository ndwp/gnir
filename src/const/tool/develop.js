export default {
  column: [
    // {
    //   label: "代码生成器",
    //   prop: "code",
    //   option: {
    //     submitText: "生成代码",
    //     column: [
    //       {
    //         label: "数据源",
    //         span: 12,
    //         row: true,
    //         // type: "password",
    //         prop: "datasourceId",
    //         rules: [
    //           {
    //             required: true,
    //             message: "请输入数据源",
    //             trigger: "blur",
    //           },
    //         ],
    //       },
    //       {
    //         label: "包名",
    //         span: 12,
    //         row: true,
    //         // type: "password",
    //         prop: "packageName",
    //         rules: [
    //           {
    //             required: true,
    //             message: "请输入包名",
    //             trigger: "blur",
    //           },
    //         ],
    //       },
    //       {
    //         label: "表名",
    //         span: 12,
    //         row: true,
    //         // type: "password",
    //         prop: "tables",
    //         rules: [
    //           {
    //             required: true,
    //             message: "请输入表名",
    //             trigger: "blur",
    //           },
    //         ],
    //       },
    //       {
    //         label: "表前缀名",
    //         span: 12,
    //         row: true,
    //         // type: "password",
    //         prop: "tablePrix",
    //       },
    //       {
    //         label: "数据库模式名",
    //         span: 12,
    //         row: true,
    //         // type: "password",
    //         prop: "schemaName",
    //       },
    //       {
    //         label: "模块名",
    //         span: 12,
    //         row: true,
    //         // type: "password",
    //         prop: "modelName",
    //       },
    //     ],
    //   },
    // },
    {
      label: "项目生成器",
      prop: "project",
      option: {
        submitText: "生成项目",
        column: [
          {
            label: "项目名",
            span: 12,
            row: true,
            // type: "password",
            prop: "projectName",
            rules: [
              {
                required: true,
                message: "请输入项目名",
                trigger: "blur",
              },
            ],
          },
          {
            label: "多模块",
            span: 12,
            row: true,
            // type: "password",
            prop: "blMultiModule",
            rules: [
              {
                required: true,
                message: "请输入多模块项目",
                trigger: "blur",
              },
            ],
          },
        ],
      },
    },
    {
      label: "pdf生成器",
      prop: "pdf",
      option: {
        submitText: "生成PDF",
        column: [
          {
            label: "标题",
            span: 12,
            row: true,
            // type: "password",
            prop: "title",
            rules: [
              {
                required: false,
                message: "请输入标题",
                trigger: "blur",
              },
            ],
          },
          {
            label: "内容",
            span: 12,
            row: true,
            // type: "password",
            prop: "context",
            rules: [
              {
                required: false,
                message: "请输入标题",
                trigger: "blur",
              },
            ],
          },
          {
            label: "图片",
            span: 12,
            row: true,
            // type: "password",
            prop: "base64",
            rules: [
              {
                required: false,
                message: "请输入base64",
                trigger: "blur",
              },
            ],
          },
        ],
      },
    },
  ],
};
