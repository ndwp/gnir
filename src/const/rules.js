// 验证规则
import {isEmail, isMobile,cardid} from "../util/validate";
let mobile = (rule, value, callback) => {
  !isMobile(value) ? callback(new Error("请输入正确的手机号格式")):callback()
};
let email = (rule, value, callback) => {
  !isEmail(value) ? callback(new Error("请输入正确的邮箱格式")):callback()
};
let card = (rule, value, callback) => {
  !cardid(value) ? callback(new Error("请输入正确的身份份证格式")):callback()
};
export default {
  mobile: [{ required: true, trigger: "blur", validator: mobile }],
  email: [{ required: true, trigger: "blur", validator: email }],
  card: [{ required: true, trigger: "blur", validator: card }],

  change10: { trigger: "change", min:3, max: 10 , message: "长度3-10字以内。",},
  change60: { trigger: "change", min:3, max: 60,  message: "长度3-60字以内。",},
  change120: { trigger: "change", min:3, max: 120, message: "长度3-120字以内。",},
}
