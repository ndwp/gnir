module.exports = {
  publicPath: process.env.VUE_APP_publicPath,
  lintOnSave: process.env.NODE_ENV !== "production",
  productionSourceMap: false,

  chainWebpack: (config) => {
    //忽略的打包文件
    config.externals({
      vue: "Vue",
      "vue-router": "VueRouter",
      vuex: "Vuex",
      axios: "axios",
      "element-ui": "ELEMENT",
    });
    const entry = config.entry("app");
    entry.add("babel-polyfill").end();
    entry.add("classlist-polyfill").end();
    entry.add("@/mock").end();
    // 给scss 加入env变量
    const oneOfsMap = config.module.rule("scss").oneOfs.store;
    oneOfsMap.forEach((itme)=> {
      itme.use("sass-loader").loader("sass-loader").options({data:"$env: '"+process.env.VUE_APP_staticUrl+"';"})
    })
  },
  devServer: {
    // 端口配置
    port: 3000,
    // proxy: {
    //   "/simple": {
    //     target: "http://localhost:8080",
    //     ws: true,
    //   },
    // pathRewrite: {
    // "^/simple": "/",
    // },
    // },
  },
};
