## 自动发布
1. 修改 `.env.porduction`,`build.sh` 中的`publicPath`
3. 在gitlab中 `CI/CD` > `Schedules` 配置轮询策略自动发布
4. 点击 `play` 按钮手动发布
## 如何启动

```
$ git clone http://192.168.102.41/ndwp-base/gungnir.git
$ cd gungnir
# 设置私服
$ npm config set registry http://192.168.102.28:8081/repository/npm-group/

# 安装
$ yarn install  
or   
$ npm install  

# 启动
$ yarn run serve  
or  
$ npm run serve  
```

## 技术支持

所有技术文档都在 Confluence 上

http://192.168.102.1:48081/x/jwEX
