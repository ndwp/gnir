#!/bin/bash
# 保持和项目里面的public path 一致多个项目不要重名
PUBLIC_PATH="gungnir"
now_time="`date +%Y%m%d`"
# 要远程发布的地址
SSH_IP="root@192.168.102.56"
# 要远程发布的地址
SSH_NGINX_PATH="/home/webapp/"

npm install
npm run build
echo "build package success"
mv dist ${PUBLIC_PATH}
zip -r ${PUBLIC_PATH}.zip ${PUBLIC_PATH}
# 上传到本地服务器
scp -r ${PUBLIC_PATH}.zip ${SSH_IP}:/home/webapp/
# 去服务器解压,解压完后考到files 目录下去保留最新版
ssh ${SSH_IP} "
              cd ${SSH_NGINX_PATH}
              unzip -o ${PUBLIC_PATH}.zip
              rm -rf ${PUBLIC_PATH}.zip
"
